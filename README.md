# AES_SecureString

Windows-Form-Application (GUI) demo of AES256 using SecureString class to encrypt password as it is input by user. 

Features:
	1. GUI to demo the Encryption and Decryption.
	
	2. Implementation of the SecureTextBox.
			This allows masking of password when being entered.
			This allows for the storage of an encrypted password and not a plaintext password. - very safe!

Branches:
	1. Master
			Tested, Release of the application
	2. Develop
			Application still in development - use at your own risk.
			

Contact
sheldon@lucidworx.co.za